type DatabaseBinding = {
    fetch: typeof fetch;
};

type SQLSuccess<T> = {
    results?: Array<T>;
    lastRowId: number | null; // doesn't work yet CFSQL-51
    changes: NonNullable<number> | 0; // still sends null CFSQL-54
    duration: NonNullable<number> | 0;
};

type SQLError = {
    error: string;
};

export class Database {
    private readonly binding: DatabaseBinding;

    constructor(binding: DatabaseBinding) {
        this.binding = binding;
    }

    prepare(query: string): PreparedStatement {
        return new PreparedStatement(this, query);
    }

    async dump(): Promise<ArrayBuffer> {
        const response = await this.binding.fetch("/dump", {
            method: "POST",
            headers: {
                "content-type": "application/json",
            },
        });
        if (response.status !== 200) {
            const err = (await response.json()) as SQLError;
            throw new Error("D1_DUMP_ERROR", {
                cause: new Error(err.error),
            });
        }
        return await response.arrayBuffer();
    }

    async batch<T>(statements: Array<PreparedStatement>): Promise<Array<SQLSuccess<T>>> {
        const exec = await this._send(
            "/query",
            statements.map((s: PreparedStatement) => s.statement),
            statements.map((s: PreparedStatement) => s.params)
        );
        return exec as Array<SQLSuccess<T>>;
    }

    async exec(query: string): Promise<object> {
        // should be /execute - see CFSQL-52
        const lines = query.trim().split("\n");
        const exec = await this._send("/query", lines, []);
        const error = exec
            .map((r: any) => {
                return r.error ? 1 : 0;
            })
            .indexOf(1);
        if (error !== -1) {
            throw new Error("D1_EXEC_ERROR", {
                cause: new Error(`Error in line ${ error + 1 }: ${ lines[error] }: ${ exec[error].error }`),
            });
        } else {
            return {
                count: exec.length,
                duration: exec.reduce((p: SQLSuccess<object>, c: SQLSuccess<object>) => {
                    return p.duration + c.duration;
                }),
            };
        }
    }

    async _send<T>(endpoint: string, query: any, params: Array<object>): Promise<any> {
        /* this needs work - we currently only support ordered ?n params */
        const body = JSON.stringify(
            typeof query == "object"
                ? (query as Array<any>).map((s: string, index: number) => {
                      return { sql: s, params: params[index] };
                  })
                : {
                      sql: query,
                      params: params,
                  }
        );

        const response = await this.binding.fetch(endpoint, {
            method: "POST",
            headers: {
                "content-type": "application/json",
            },
            body,
        });
        if (response.status !== 200) {
            const err = (await response.json()) as SQLError;
            throw new Error("D1_ERROR", { cause: new Error(err.error) });
        }

        const answer = await response.json();

        return Array.isArray(answer) ? (answer as Array<SQLSuccess<T>>) : (answer as SQLSuccess<T>);
    }
}

class PreparedStatement {
    readonly statement: string;
    private readonly database: Database;
    params: Array<any>;

    constructor(database: Database, statement: string, values?: any) {
        this.database = database;
        this.statement = statement;
        this.params = values || [];
    }

    bind(...values: any) {
        // CFSQL-55 - Validate value types
        return new PreparedStatement(this.database, this.statement, values);
    }

    async first(colName?: string): Promise<object|null> {
        const info = await this.database._send<Array<object>>("/query", this.statement, this.params);
        const results = info.results;
        if (results.length < 1) {
            return null;
        }
        const result = results[0];
        if (colName !== undefined) {
            if (result[colName] === undefined) {
                throw new Error("D1_COLUMN_NOTFOUND", {
                    cause: new Error(`Column not found`),
                });
            }
            return result[colName];
        } else {
            return result;
        }
    }

    async run(): Promise<SQLSuccess<void>> {
        return this.database._send("/execute", this.statement, this.params);
    }

    async all(): Promise<SQLSuccess<Array<object>>> {
        return await this.database._send<Array<object>>("/query", this.statement, this.params);
    }

    async raw(): Promise<Array<any>> {
        const s = await this.database._send<Array<object>>("/query", this.statement, this.params);
        const raw = [];
        for (var r in s.results) {
            const entry = Object.keys(s.results[r]).map((k) => {
                return s.results[r][k];
            });
            raw.push(entry);
        }
        return raw;
    }
}
